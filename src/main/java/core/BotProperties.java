package core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BotProperties {


    String token;
    String username;
    Set<Long> allowedIdSet;

    BotProperties() throws IOException {
        InputStream resourceAsStream = BotProperties.class.getResourceAsStream("../bot.properties");
        Properties internalProperties = new Properties();
        internalProperties.load(resourceAsStream);
        token = internalProperties.getProperty("token");
        username = internalProperties.getProperty("username");
        allowedIdSet = Stream.of(internalProperties.getProperty("allowed_ids").split(",")).map(Long::valueOf).collect(Collectors.toSet());
    }


}
