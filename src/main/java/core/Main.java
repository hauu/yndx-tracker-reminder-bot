package core;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

@Configuration
public class Main {

    public static void main(String[] args) {
        System.out.println(Main.class.getClassLoader().getResource("/"));
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("core","tracker");

        try {
            telegramBotsApi.registerBot(applicationContext.getBean(Bot.class));
        } catch (BeansException e) {
            e.printStackTrace();
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }



}
