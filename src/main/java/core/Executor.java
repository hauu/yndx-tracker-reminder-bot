package core;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import tracker.FindTicket;

@Service
public class Executor implements ApplicationContextAware {

    @Autowired
    BotProperties properties;
    private ApplicationContext applicationContext;

    public Verdict setVerdict(Update update) {

        if (isValid(update.getMessage())) {
            return authorize(update);
        } else {
            return reject(update);
        }
    }

    private Verdict reject(Update update) {
        return new Verdict(update, "Извините, мне не разрешают разговаривать с незнакомцами");
    }

    private Verdict authorize(Update update) {
        ResponseEntity<String> springAnswer = applicationContext.getBean(FindTicket.class).getAllTickets();
        return new Verdict(update, "Пока что я умею отдавать содержимое очереди TEST, вот оно:\n\n" + springAnswer.toString());
    }

    private boolean isValid(Message message) {
        long chatId = message.getChatId();
        if (properties.allowedIdSet.contains(chatId)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;
    }
}
