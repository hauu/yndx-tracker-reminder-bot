package core;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;


public class Verdict {

    private SendMessage sendMessage;

    public SendMessage getSendMessage() {
        return sendMessage;
    }

    Verdict(Update update) {
        sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Я тебя узнал, ты номер " + update.getMessage().getChatId());
    }

    Verdict(Update update, String text) {
        sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText(text);
    }
}
