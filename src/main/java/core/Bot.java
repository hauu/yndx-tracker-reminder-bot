package core;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Service
public class Bot extends TelegramLongPollingBot implements ApplicationContextAware {

    @Autowired
    private BotProperties properties;
    private ApplicationContext applicationContext;

    public void onUpdateReceived(Update update) {

        Executor executor = applicationContext.getBean(Executor.class);
        Verdict verdict = executor.setVerdict(update);
        SendMessage sendMessage = verdict.getSendMessage();
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void onUpdatesReceived(List<Update> updates) {
        updates.forEach(this::onUpdateReceived);
    }

    public String getBotUsername() {
        return properties.username;
    }

    public String getBotToken() {
        return properties.token;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
