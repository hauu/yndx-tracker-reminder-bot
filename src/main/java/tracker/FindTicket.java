package tracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class FindTicket extends TrackerAction {

    public ResponseEntity<String> getAllTickets()
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "OAuth " + trackerProperties.token);
        httpHeaders.set("X-Org-Id", String.valueOf(trackerProperties.orgId));
        httpHeaders.set("Content-Type", "application/json");

        Map<String, String> map= new HashMap<>();
        map.put("queue", "TEST");

        HttpEntity<Map<String, String>> request = new HttpEntity<>(map, httpHeaders);


        ResponseEntity<String> responseEntity = restTemplate.postForEntity(trackerProperties.host+"/v2/issues/_search", request, String.class);
        return responseEntity;
    }
}
