package tracker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Service
public class TrackerProperties {
    long orgId;
    String token;
    HashMap<Long, String> users;
    Map<String, Intent> intents;
    String host;

    TrackerProperties() throws IOException {

        InputStream resourceAsStream = TrackerProperties.class.getResourceAsStream("../tracker.properties");
        Properties internalProperties = new Properties();
        internalProperties.load(resourceAsStream);
        token = internalProperties.getProperty("tracker_api_token");
        host = internalProperties.getProperty("tracker_host");
        orgId = Long.valueOf(internalProperties.getProperty("org_id"));
        ObjectMapper objectMapper = new ObjectMapper();
        users = objectMapper.readValue(internalProperties.getProperty("tracker_users_json"), new TypeReference<Map<Long,String>>(){});

        intents = new HashMap<>();
        intents.put("послушать", new Intent(Queue.FUN, Tag.MUSIC));
        intents.put("посмотреть", new Intent(Queue.FUN, Tag.MOVIE));
        intents.put("купить", new Intent(Queue.HOME, Tag._NONE));
    }

    /*
    В запросах к API Трекера указывайте заголовки:
    Authorization: OAuth <ваш OAuth-токен>.
    X-Org-Id: <идентификатор вашей организации>
     */
}